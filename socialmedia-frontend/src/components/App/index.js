import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import './App.css';
import Login from '../Login';
import Members from '../Members';

class App extends Component{
  state = {
    clientToken: ''
  }

  callbackGetToken = (token) => {
    this.setState({clientToken: token});
  }


  render(){
    return(
      <Switch>
        <Route exact path="/" component={props => <Login sendToken = {this.callbackGetToken}/>}/>
        <Route path="/Members" component={props => <Members authToken = {this.state.clientToken}/>}/>
      </Switch>
    )
  }
};

export default App;
