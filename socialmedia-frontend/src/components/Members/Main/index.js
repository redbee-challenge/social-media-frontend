import React, { Component } from "react";
import { Route, withRouter } from "react-router-dom";
import "./Main.css";
import Tweets from "../Tweets";

class Main extends Component  {
    componentWillReceiveProps(nextProps){
        console.log("Main receiving nextprops");
    }

    render(){
        return(
            <div>
                <Route exact path={this.props.match.url + "/"} component={props => <Tweets authToken = {this.props.authToken} interestList = {this.props.interestList}/>}/>
                <Route path={this.props.match.url + "/Tweets/:id"} component={props => <Tweets authToken = {this.props.authToken} interestList = {this.props.interestList}/>}/>
            </div>
        )
    }

}

export default withRouter(Main);