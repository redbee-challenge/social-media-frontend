import React, {Component} from "react";

const tweetsUrl = "http://localhost:8080/socialmedia/rest/tweetsAPI/fetchTweetsForInterest";

class Tweets extends Component{
    constructor(props){
        super(props);
        this.state = {
            jsonTweets: []
        }
    }
    
    componentDidMount(){
        this.props.interestList.map((interest) => {
            fetch(tweetsUrl, {
                method: 'POST',
                headers: {
                "Content-Type": "application/json",
                "Authorization": this.props.authToken
                },
                body: JSON.stringify(interest)
            })
            .then(response => response.json())
            .then(json => {
                this.setState(prevState => ({
                    jsonTweets: [...prevState.jsonTweets, json]
                }));
            })
            .catch(error => {console.log("Error: " + error)})
        })
    
    }


    render(){
        // console.log("Tweets:: " + this.state.jsonTweets.length);
        const tweets = this.state.jsonTweets.map((interestRelatedTweets) => (
            interestRelatedTweets
        ));

        return (
            <div>
                <p>All Tweets Page</p>

                {this.state.jsonTweets.length}
                {tweets}
                {this.state.jsonTweets.map((interestRelatedTweets, index) => (
                    interestRelatedTweets.map((tweet, index2) => (
                        <div key={index.toString + index2.toString}>
                            <p>*********</p>
                            <p>{index}{index2}</p>
                        </div>
                    ))
                ))}
            </div>
        )
    }
}

export default Tweets;