import React, { Component } from "react";
import "./Members.css";
import Menu from "./Menu";
import Main from "./Main";

const interestUrl = "http://localhost:8080/socialmedia/rest/interestAPI/fetchAllInterests";


class Members extends Component{
    constructor(props){
        super(props);
        this.state = {
            interests: [],
            jsonTweets: ""
        }
    }
    
    componentDidMount(){
        fetch(interestUrl, {
            method: 'GET',
            headers: {
              "Content-Type": "application/json",
              "Authorization": this.props.authToken
            }
        })
        .then(response => response.json())
        .then(json => {
            this.setState({interests: json});  
        })
        .catch(error => {console.log("Error: " + error)})
    };

    render(){
        return(
            <div className="Members-body">
                <div className="Menu-sidebar">
                    <Menu interestList = {this.state.interests}/>
                </div>
                <div className="Main-container">
                    <Main authToken = {this.props.authToken} interestList = {this.state.interests}/>
                </div>
            </div>
        )
    }

}
export default Members;