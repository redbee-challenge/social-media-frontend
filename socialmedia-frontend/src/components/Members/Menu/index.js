import React, { Component } from "react";
import './Menu.css';
import {Link} from "react-router-dom";


class Menu extends Component {
    constructor(props){
        super(props);
    }

    componentWillReceiveProps(nextProps){
        this.interests = nextProps.interestList;
    }

    render() {
        // console.log(this.props);
        const listItems = this.interests 
            ? this.interests.map((interest) => (
                // <Link to={`../Members/initial`}>
                <Link to={`/Members/Tweets/${interest.id}`}>
                    <li key={interest.id}>{interest.type + interest.keyword}</li>
                </Link>
            )) 
            : [];
        return(
            <div className="Menu-container">
                <h2>Menu</h2>
                {listItems.length > 0 &&
                    <div>
                        <p>Intereses</p>
                        <ul>
                            {listItems}
                        </ul>
                    </div>
                }
            </div>
        )
    }
}

export default Menu;