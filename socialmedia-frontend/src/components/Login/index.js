import React, { Component } from 'react';
import './Login.css';
import {Link, Redirect } from "react-router-dom";

const loginFormurl = "http://localhost:8080/socialmedia/rest/loginAPI/login";

class Login extends Component {

  state = {
    credentials:{
      "username": "", 
      "password": ""
    },
    clientToken: ""
  }

  constructor(props){
    super(props);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }
        
  handleUsernameChange(event){
    let credentials = Object.assign({}, this.state.credentials);
    credentials.username = event.target.value;
    this.setState({credentials});
  }


  handlePasswordChange(event){
    let credentials = Object.assign({}, this.state.credentials);
    credentials.password = event.target.value;
    this.setState({credentials});
  }

  handleFormSubmit(event){
    event.preventDefault();
    const data = JSON.stringify(this.state.credentials);
    
    fetch(loginFormurl, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: data,
    })
    .then((response) => {
      if(response.ok){
        const token = response.headers.get('Authorization');
        console.log(token);
        this.setState({clientToken: token});
        this.props.sendToken(token);
      }else{
        console.log(response.statusText);
      }
    })

    .catch(function(error) {
      console.log(error);
    });
  }

  render() {
    return (
      <div className="App">
        <Link to={"/Members"}>
          Miembros
         </Link>
        
        <h1 className="Login-title">Login to Social Media Aggregator</h1>
        <form className="Login-box" onSubmit={this.handleFormSubmit}>
          <p>
            <label>
              Username
              <input id="username" type="text" name="username" required onChange={this.handleUsernameChange}/>
            </label>
          </p>
          <p>
            <label>
              Password
              <input id="password" type="password" name="password" autoComplete="password" required  onChange={this.handlePasswordChange}/>
            </label>
          </p>
          <p><input type="submit" value="Login"/></p>
        </form>
      </div>
    );
  }
}

export default Login;
